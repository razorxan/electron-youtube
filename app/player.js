const $ = require('jquery')
const Player = require('./assets/js/classes/Player')
const WaveVisualizer = require('./assets/js/classes/WaveVisualizer')
const BarVisualizer = require('./assets/js/classes/BarVisualizer')
const player = new Player()


const item_html = `
<div class="box_single">
	<div class="b_thumb">
		<img src="assets/img/bk.jpg">
	</div>
	<div class="title">
		I Puffi sono Belli e Canterini alba bla bla bla
	</div>
	<div class="cancel" style="top:18px;"></div>
</div>`

player.on('enqueue', data => {
	const item = $(item_html)
	item.data('id', data.item._id).addClass('active')
	item.find('.cancel').click(function (e) {
		e.stopPropagation()
		const id = $(this).closest('.box_single').data('id')
	})
	item.find('.title').text(data.item.meta.title)
	item.find('img').attr('src', data.item.meta.thumbnail)
	item.click(function () {
		player.play($(this).index())
	})
	$('.playlist-box').append(item)
	if (data.index === 0) player.play()
	
}).on('timeupdate', time => {
	const perc = (time / player.duration) * 100
	const current = (new Date).clearTime().addSeconds(time.toFixed(0)).toString('m:ss')
	const left = (new Date).clearTime().addSeconds(player.duration.toFixed(0) - time.toFixed(0)).toString('m:ss')
	$('.current-time').text(current)
	$('.duration').text(left)
	$('.progress').stop(true).animate({width: perc + '%'})
}).on('durationchange', e => {
	const current = (new Date).clearTime().addSeconds(player.duration).toString('m:ss')
	$('.duration').text(current)
}).on('seeked', e => {
	const perc = (player.audio.currentTime / player.duration) * 100
	$('.progress').stop(true).css({width: perc + '%'})
}).on('play', index => {
	const thumb = player.playlist[index].meta.thumbnail
	$('.playpause').removeClass('play').addClass('pause')
	$('.image').empty().append($('<img />').attr('src', thumb))
	$('h1').text(player.playlist[index].meta.title)
	$('.playlist-box > .box_single:eq(' + index + ')').prepend($('.playlist_visualizer'))
}).on('pause', () => {
	$('.playpause').removeClass('pause').addClass('play')
}).on('speaker_connected', () => {
	$('.speaker').addClass('active');
}).on('speaker_disconnected', () => {
	$('.speaker').removeClass('active');
})


const visualizer = new WaveVisualizer(player.source, $('.visualizer')[0])

const playlist_visualizer = new BarVisualizer(player.source, $('.playlist_visualizer')[0])

module.exports = player