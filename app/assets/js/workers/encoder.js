const WebEmitter = require('web-emitter');
const lamejs = require('lamejs');

const worker = new WebEmitter();

var sample_rate = 48000;
var mp3encoder = new lamejs.Mp3Encoder(2, sample_rate, 128);

var mp3Buffer = [];

worker.on('mp3', data => {
	var samples = new Int16Array(2048);
	var left = convertFloat32ToInt16(data.channels[0]);
	var right = convertFloat32ToInt16(data.channels[1]);
	var buffer = mp3encoder.encodeBuffer(left, right);
	//mp3Buffer.push(buffer);
	worker.emit('encoded', {buffer});
});

function convertFloat32ToInt16(inFloat) {
	var sampleCt= inFloat.length;
	var outInt16= new Int16Array(sampleCt);
	for(var n1=0;n1<sampleCt;n1++) {
		//This is where I can apply waveform modifiers.
		var sample16= 0x8000*inFloat[n1];
		//Clamp value to avoid integer overflow, which causes audible pops and clicks.
		sample16= (sample16 < -32767) ? -32767 : (sample16 > 32767) ? 32767 : sample16;
		outInt16[n1]= sample16;
	}
	return(outInt16);
}

function convertFloat32ToInt16a (buffer) {
	var l = buffer.length;
	var out = new Int16Array(l);
	for(var i=0; i < l; i++) {
		//This is where I can apply waveform modifiers.
		var sample = 0x8000 * buffer[i];
		//Clamp value to avoid integer overflow, which causes audible pops and clicks.
		sample = (sample < -32767) ? -32767 : (sample > 32767) ? 32767 : sample;
		out[i]= sample;
	}
	return(out);
}