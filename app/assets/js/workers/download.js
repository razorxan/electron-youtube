const path              = require('path')
const fs                = require('fs')
const YoutubeDownloader = require('yt-downloader')
const WebEmitter        = require('web-emitter')
const Mp3Conversion     = require(path.join(__dirname, '..', '/classes/Mp3Conversion'))
const __unpacked        = __dirname.replace('app.asar', 'app.asar.unpacked')
const isWin             = process.platform === "win32"
const __ffmpeg_path     = path.resolve(__unpacked, '..', '..', '..', 'bin', 'ffmpeg' + (isWin ? '.exe' : ''))

const worker = new WebEmitter()
const DL = new YoutubeDownloader()

DL.on("enqueue", item => {
    const dl = item.dl
    dl.on("loading", url => {
        worker.emit('loading', dl.id, url)
    }).on("start", data => {
        worker.emit("downloadstart", dl.id, data)
        console.log("started", data.meta.title)
    }).on("progress", progress => {
        worker.emit("downloadprogress", dl.id, progress)
        console.log(dl.meta.title, progress)
    }).on("end", data => {
        worker.emit("downloadend", dl.id, data)
        const source_path = data.filename
        const output_path = data.filename.substring(0, data.filename.lastIndexOf('.')) + '.mp3'
        const mp3 = new Mp3Conversion(source_path, output_path, __ffmpeg_path)
        mp3.on("start", () => {
            worker.emit("conversionstart", dl.id, output_path)
        }).on("progress", progress => {
            worker.emit("conversionprogress", dl.id, progress)
            console.log("Conversion of " + dl.meta.title, progress)
        }).on("end", filename => {
            worker.emit("conversionend", dl.id, output_path, data.meta)
            fs.unlinkSync(data.filename)
        }).start().then(() => {
            console.log("then")
        }).catch(error => {
            worker.emit("conversionerror", dl.id, error)
        })
    })
})
worker.on("add_download", url => {
    DL.add(url)
})