const icon = '../../icon/icon.png'
const WebEmitter = require('web-emitter')

const we = new WebEmitter()

we.emit('hello', 'emilio', 'carrino')
setTimeout(() => {
    we.emit('timeout', {
        text: 'ciao come va?'
    })
}, 2000)
we.on('text', (text, meta) => {
    console.log(text, meta)
})
/*
self.addEventListener('message', e => {
    let mysNotification = new Notification(e.data.title, {
        body: e.data.body,
        icon: e.data.icon || icon
    })
})

self.postMessage('hello')
*/