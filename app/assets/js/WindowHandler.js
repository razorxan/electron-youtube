class WindowHandler
{

	constructor (win)
	{
		this.win = win
		this.mazimized = false
		this.fullscreen = false
	}

	toggleMaximize ()
	{
		if (this.maximized) {
			this.maximized = false
			this.win.unmaximize()
			return this
		}
		this.win.maximize()
		this.maximized = true
		return this
	}

	isMinimized ()
	{
		return this.win.isMinimized()
	}

	minimize ()
	{
		this.win.minimize()
		return this
	}

	close ()
	{
		this.win.hide()
		return this
	}

	toggleFullscreen ()
	{

		if (this.fullscreen) {
			this.win.setFullScreen(false)
			this.fullscreen = false
			return this
		}
		this.win.setFullScreen(true)
		this.fullscreen = true
		return this
	}

}

module.exports = WindowHandler
