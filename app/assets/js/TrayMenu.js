module.exports = (app) => {
    const MenuTemplate = [
        {
            label: 'Quit',
            click: () => {
                app.quit()
            }
        }
    ]
    return MenuTemplate
}