const { EventEmitter } = require('events')
const ffmpeg           = require('fluent-ffmpeg')

class Mp3Conversion extends EventEmitter
{
    constructor (input, output, ffmpeg_path)
    {
        super()
        this.input = input
		this.output = output
		ffmpeg.setFfmpegPath(ffmpeg_path)
    }

    start ()
    {
		return new Promise((resolve, reject) => {
			this.emit('start')
            this.command = ffmpeg(this.input)
                    .audioCodec('libmp3lame')
                    .noVideo()
					.output(this.output)
			this.command.on('progress', data => {
                if (!data.percent) return
                if (data.percent > 100) data.percent = 100
				this.emit('progress', data.percent.toFixed(2))
			})
			this.command.on('end', () => {
				resolve(this.output)
				this.emit('end', this.output)
			})
			this.command.on('error', error => {
				reject(error)
			})
			this.command.run()
		})
        
    }

}

module.exports = Mp3Conversion