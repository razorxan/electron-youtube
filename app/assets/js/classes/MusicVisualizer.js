class MusicVisualizer
{

	constructor (source, container)
	{
		this.source = source
		this.container = container
		this.audio_context = this.source.context

		this.fps_sample = []
		this.fps_sample_size = 60
		this.fps_sample_index = 0
		this.fps = 0
		this.last = null
		this.frame_rate = 60

		this.animation_interval = null
		this.canvas = document.createElement('canvas')
		this.context = this.canvas.getContext('2d')
		this.container.appendChild(this.canvas)

		this.analyser = this.audio_context.createAnalyser()
		this.analyser.smoothingTimeConstant = .85
		this.analyser.minDecibels = -90
		this.analyser.maxDecibels = -10
		this.analyser.fftSize = 1024
		this.source.connect(this.analyser)

		this.volume = 0
		this.frequency_data_length = this.analyser.frequencyBinCount
		this.frequency_data = new Uint8Array(this.frequency_data_length)
		this.byte_time_domain_data = new Uint8Array(this.frequency_data_length)
		this.process_data_interval = 1000 / 200


		//HELPERS
		this.floor = Math.floor
	    this.round = Math.round
	    this.random = Math.random
	    this.sin = Math.sin
	    this.cos = Math.cos
	    this.PI = Math.PI
	    this.PI_TWO = this.PI * 2
	    this.PI_HALF = this.PI / 180

		this.init()
	}

	init ()
	{
		window.addEventListener('resize', this.resizeCanvas.bind(this), false)
		this.resizeCanvas()
		//this.draw()

		requestAnimationFrame(this.step.bind(this))
		//this.animation_interval = setInterval(this.step.bind(this), 1000 / this.frame_rate)
		this.process_data_interval = setInterval(this.dataProcess.bind(this), this.process_data_interval)
	}

	step ()
	{
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
		this.draw()
		this.calculateFPS()
		requestAnimationFrame(this.step.bind(this))
	}

	draw ()
	{
		// override this
	}

	dataProcess()
	{
		this.analyser.getByteTimeDomainData(this.byte_time_domain_data)
		this.analyser.getByteFrequencyData(this.frequency_data)
		let total = 0
		this.frequency_data.forEach(v => {
			total += v
		})
		this.volume = total
	}

	calculateFPS ()
	{
		const now = Date.now()
		if(!this.last) {
			this.last = now
			this.fps = 0
			return
		}
		const delta = (now - this.last) / 1000
		const temp_fps = 1 / delta
		this.last = now
		this.fps_sample[this.fps_sample_index++] = temp_fps
		if (this.fps_sample_index === this.fps_sample_size) this.fps_sample_index = 0
		if (this.fps_sample.length < this.fps_sample_size) {
			this.fps = 0
		} else {
			let sum = 0
			for (let i = 0; i < this.fps_sample_size; i++) {
				sum += this.fps_sample[i]
			}
			this.fps = Math.round(sum / this.fps_sample_size)
		}
	}

	resizeCanvas ()
	{
		this.canvas.width = this.container.offsetWidth
		this.canvas.height = this.container.offsetHeight
	}

}

module.exports = MusicVisualizer