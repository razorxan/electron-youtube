const request = require('request');
const { PassThrough } = require('stream');
const toBuffer = require('typedarray-to-buffer');

class Streamer extends require('events').EventEmitter
{
	constructor (encoder, url = null)
	{
		super();
		this._url = url;
		this._request = null;
		this._pass = null;
		this._encoder = encoder
	}

	url (url)
	{
		this._url = url;
		return this;
	}

	start (url = null)
	{
		if (url) {
			this.url(url);
		}
		this.stop();
		this._pass = new PassThrough();
		this._request = request.post(this._url);
		this._pass.pipe(this._request);
		this._encoder.on('encoded', this.write.bind(this));
		return this;
	}

	stop ()
	{
		if (this._pass) {
			this._pass.destroy();
			this._pass = null;
		}
		if (this._request) {
			this._request.end();
			this._request = null;
		}
		this._encoder.removeAllListeners('encoded');
		return this;
	}

	write (chunk)
	{
		if (this._pass) {
			return this._pass.write(toBuffer(chunk.buffer));
		}
		return false;
	}

}


module.exports = Streamer;