const request = require('request')

class Youtube 
{
    constructor (apiKey)
    {
        this.apiKey = apiKey
    }

    search (q, max = 30)
    {
        const url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&key=' + this.apiKey + '&maxResults=' + max +'&type=video&q=' + q
        return this._request(url)
    }

    get (id)
    {
        const url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&key=' + this.apiKey + '&maxResults=1&id=' + id
        return this._request(url)
    }

    _request (url)
    {
        return new Promise((resolve, reject) => {
            request(url, (error, response, body) => {
                if (error) reject(error)
                else resolve(JSON.parse(body).items)
            })
        })
    }
    
    match (url)
    {
        const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
        const match = url.match(regExp)
        const id = (match&&match[7].length==11)? match[7] : false
        if (id) return Promise.resolve(id)
        return Promise.reject('Url not valid')
    }

}

module.exports = Youtube