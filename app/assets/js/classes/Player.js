const {EventEmitter} = require('events')

class Player extends EventEmitter
{
	constructor ()
	{
		super()
		this._setVariables()
		this._setWebAudioApi()
		this._registerEvents()
	}

	_setVariables ()
	{
		this.audio = new Audio()
		
		this.playlist = []
		this.duration = 0
		this.time = 0
        this.status = 0 //0 stopped, 1 paused, 2 playing
		this._mode = 0 //0 default, 1 repeat all, 2 repeat
		this.index = 0 //playlist index
		this._volume = 1
		this._listeners = []
		this._playbackrate = 1
		this.audio.playbackRate = 1
		this.audio.autoplay = false
		this.audio.preload = 'auto'
        this.audio.autobuffer = true
        this.context = null;
	}

	_setWebAudioApi ()
	{
		this.context = new AudioContext()
		this.source = this.context.createMediaElementSource(this.audio)
		this._gain = this.context.createGain()
		this._gain.gain.value = 1
		this.processor = this.context.createScriptProcessor(2048, 2, 2)
		this.processor.onaudioprocess = this.processaudio.bind(this)
		this.mixer = this.context.createGain()
		
		this.source.connect(this._gain)
		this._gain.connect(this.processor)
		this.processor.connect(this.context.destination)
		this._gain.connect(this.context.destination)
	}

	_registerEvents ()
	{
		this.audio.onprogress = e => {
			var ranges = []
			for (let i = 0; i < this.audio.buffered.length; i++) {
				const range = {
					start: this.audio.buffered.start(i),
					end: this.audio.buffered.end(i)
				}
				ranges.push(range)
			}
			this.emit(e.type, [e, ranges])
		}
		this.audio.onload = e => {
			this.emit(e.type, e)
		}
		this.audio.onloadstart = e => {
			this.emit(e.type, e)
		}
		this.audio.oncanplay = e => {
			this.emit(e.type, e)
		}
		this.audio.onplay = e => {
            this.status = 2
			this.emit(e.type, this.index)
		}
		this.audio.onplaying = e => {
			this.emit(e.type, [e, this.index])
		}
		this.audio.onpause = e => {
            this.status = 1
			this.emit(e.type, e)
		}
		this.audio.oncanplaythrough = e => {
			this.emit(e.type, e)
		}
		this.audio.onvolumechange = e => {
			this.emit(e.type, [this.audio.volume, this._volume])
		}
		this.audio.onloadeddata = e => {
			this.emit(e.type, e)
		}
		this.audio.onplaying = e => {
			this.emit(e.type, e)
		}
		this.audio.onseeked = e => {
			this.emit(e.type, e)
		}
		this.audio.ontimeupdate = e => {
			this.time = this.audio.currentTime
			this.emit(e.type, this.audio.currentTime)
		}
		this.audio.ondurationchange = e => {
			this.duration = this.audio.duration
			this.emit(e.type, e)
		}
		this.audio.onended = (e) => {
			switch (this._mode) {
				case 0:
					if (this.index + 1 < this.playlist.length) {
						this.next()
					} else {
						this.stop()
					}
					break
				case 1:
					this.next()
					break
				case 2:
					this.stop().play()
					break
			}
		}
		return this
	}

	processaudio (e)
	{
		this.emit('audiodata', e)
	}

	load (url)
	{
		this.audio.pause()
		this.audio.src = url
		return this
	}

	play (i = null)
	{
		if (i !== null) {
			let index = i
			if (index > -1 && index < this.playlist.length) {
				this.index = index
				this.load(this.playlist[this.index].path)
				setTimeout(() => {
					this.audio.play()
				}, 150)
			}
		} else if (this.index > -1 &&  this.index < this.playlist.length) {
			if (this.status !== 1) {
				this.load(this.playlist[this.index].path)
			}
			setTimeout(() => {
				this.audio.play()
			}, 150)
		}
		return this
	}

	pause ()
	{
		this.audio.pause()
		this.emit('pause')
		return this
	}

	playPause ()
	{
		if (this.status !== 2) this.play()
		else this.pause()
		return this
	}

	stop ()
	{
		this.pause()
        this.seek(0)
        this.status = 0
		this.emit('stop')
		return this
	}

	seek (t)
	{
		for (let i = 0; i < this.audio.seekable.length; i++) {
			if (t >= this.audio.seekable.start(i) && t <= this.audio.seekable.end(i)) {
				this.audio.currentTime = t
				this.emit('seek', t)
			}
		}
		return this
	}

	next ()
	{
		this.index = (this.index + 1) % this.playlist.length
		this.play(this.index)
		return this
	}

	previous ()
	{
		if (this.index === 0) {
			this.index = this.playlist.length - 1
		} else {
			this.index--
		}
		this.play(this.index)
		return this
	}

	volume (v = null)
	{
		if (v && parseFloat(v) <= 1 && parseFloat(v) >= 0) {
			this.audio.volume = v
			this.emit('volume', this.audio.volume)
			this._volume = this.audio.volume
			return this
		}
		return this._volume;
	}

	gain (v = null)
	{
		if (v && parseFloat(v) <= 1 && parseFloat(v) >= 0) {
			this._gain.gain.value = v
			this.emit('gain', this._gain.gain.value)
			return this
		}
		return this._gain.gain.value;
	}

	playbackRate (rate = null)
	{
		if (rate && parseFloat(rate) <= 4 && parseFloat(rate) >= 0.5) {
			if (this._playbackRate !== rate) {
				this.audio.playbackRate = this._playbackRate = rate
				this.emit('rate', this.audio.playbackRate)
			}
			return this
		}
		return this._playbackRate
	}

	mode (m = null)
	{
		if (m && parseInt(m, 10) <= 2 && parseInt(m, 10) >= 0) {
			if (this._mode !== m) {
				this._mode = m
				this.emit('modechange', this._mode)
			} else {
				this._mode = m
			}
			return this
		}
		return this._mode
	}

	enqueue (item)
	{
		const items = []
		if (Array.isArray(item)) {
			items = item
		} else {
			items.push(item)
		}
		items.forEach((value , index) => {
			this.playlist.push(value)
			this.emit('enqueue', {
				item: value,
				index: this.playlist.length - 1
			})
		})
		return this
	}

	remove (index)
	{
		//event remove?
		if (this.index === index) {
			//this.next();
		}
		this.playlist.splice(index, 1)
		this.emit('remove', index)
		return this
	}

	speaker (s)
	{
		if (s) {
			this._gain.connect(this.context.destination)
			this.emit('speaker_connected')
		} else {
			this._gain.disconnect(this.context.destination)
			this.emit('speaker_disconnected')
		}
		return this
	}

}

module.exports = Player