const fs = require('fs')
const path = require('path')
const __unpacked = __dirname.replace('app.asar', 'app.asar.unpacked')
const {remote} = require('electron')
const app = remote.app

class Settings
{
	constructor (filename = 'default.json')
	{
		this._path = path.resolve(app.getAppPath(), 'app', 'config', filename).replace('app.asar', 'app.asar.unpacked')
		this.params = {
			alwaysOnTop: true,
			autoPlay: false,
			volume: 0.3,
			notifications: false,
			startOnTray: true,
			startIndex: 3
		}
		if (fs.existsSync(this._path)) {
			let p = null
			try {
				p = JSON.parse(fs.readFileSync(this._path))
			} catch (e) {
				p = {}
			}
			Object.assign(this.params, p)
		} else {
			fs.writeFileSync(this._path, '')
		}
	}

	get (prop)
	{
		return this.params[prop]
	}

	set (prop, value)
	{
		this.params[prop] = value
		return this._save()
	}

	_save ()
	{
		return new Promise((resolve, reject) => {
			fs.writeFile(this._path, JSON.stringify(this.params), err => {
				if (err) {
					reject(err)
				} else {
					resolve(true)
				}
			})
		})
	}

}

module.exports = Settings