'use strict'

//import MusicVisualizer from 'MusicVisualizer'

const MusicVisualizer = require('./MusicVisualizer')

class WaveVisualizer extends MusicVisualizer {

	constructor (source, container)
	{
		super(source, container)
		this.waveform_color = "rgba(29, 36, 57, 0.05)"
	    this.waveform_color_2 = "rgba(0,0,0,0)"
	    this.waveform_line_color = "rgba(157, 242, 157, 0.11)"
	    this.waveform_line_color_2 = "rgba(157, 242, 157, 0.8)"
	    this.waveform_tick = 0.05
	    this.total_points = this.analyser.fftSize / 2
	    this.points = []
		this.avg = 0
	}

	filter (f)
	{

	}

	getAverage ()
	{
		const values = [].slice.call(this.frequency_data, 0, 80)
		let value = 0;
		values.forEach(v => {
	    	value += v;
		})
		this.avg = value / values.length;
	}

	draw ()
	{
		const dataArray = this.frequency_data.slice(0, 40)
		const bufferLength = this.analyser.frequencyBinCount
		this.context.fillStyle = 'rgba(0, 177, 206, 0.2)'

		
		this.context.shadowBlur = 10
		this.context.shadowColor = 'rgba(0, 177, 206, 0.2)'
		const sliceWidth = this.canvas.width * 1.0 / (bufferLength - 1)
		
		const bars = Math.min(dataArray.length, 80)
		const width = this.canvas.width / bars
		for (let i = 0; i < bufferLength; i++) {
			const height = (dataArray[i] / 255) * (this.canvas.height / 2) * (this.volume / 20400)
			const x = i * width
			this.context.fillRect(x, this.canvas.height - height, width, height);
		}

	}

	drawLine (offset = 0)
	{
		
	}

}

module.exports = WaveVisualizer