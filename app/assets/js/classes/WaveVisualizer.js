'use strict'

//import MusicVisualizer from 'MusicVisualizer'

const MusicVisualizer = require('./MusicVisualizer')

class WaveVisualizer extends MusicVisualizer {

	constructor (source, container)
	{
		super(source, container)
		this.waveform_color = "rgba(29, 36, 57, 0.05)"
	    this.waveform_color_2 = "rgba(0,0,0,0)"
	    this.waveform_line_color = "rgba(157, 242, 157, 0.11)"
	    this.waveform_line_color_2 = "rgba(157, 242, 157, 0.8)"
	    this.waveform_tick = 0.05
	    this.total_points = this.analyser.fftSize / 2
	    this.points = []
        this.avg = 0
        this._step = function () {}
	}

	filter (f)
	{

	}

	getAverage ()
	{
        const values = [].slice.call(this.frequency_data, 0, 80)
		let value = 0;
		values.forEach(v => {
	    	value += v;
		})
		this.avg = value / values.length;
    }
    
    onStep (f)
    {
        this._step = f
    }

	draw ()
	{
        this.getAverage()
        this._step.call(this, [this.avg])
		const dataArray = this.byte_time_domain_data.slice()
		const bufferLength = this.analyser.frequencyBinCount
		this.context.fillStyle = 'white'

		this.context.lineWidth = 2
		this.context.strokeStyle = 'white'

		this.context.beginPath()
		this.context.shadowBlur = 10
		this.context.shadowColor = 'white'
		const sliceWidth = this.canvas.width * 1.0 / (bufferLength - 1)
		let x = 0

		for (let i = 0; i < bufferLength; i++) {
			let v = dataArray[i] / 128.0
			let y = v * this.canvas.height / 2
			if (i === 0) {
				this.context.moveTo(x, y)
			} else {
				this.context.lineTo(x, y)
			}
			x += sliceWidth
		}
		this.context.stroke()

	}

	drawLine (offset = 0)
	{
		
	}

}

module.exports = WaveVisualizer