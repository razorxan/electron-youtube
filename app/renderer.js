const path                             = require('path');
const fs                               = require('fs');
const {remote, ipcRenderer, clipboard} = require('electron');
const WindowHandler                    = require('./assets/js/WindowHandler');
const Youtube                          = require('./assets/js/classes/Youtube');
const WebEmitter                       = require('web-emitter');
const Streamer                         = require('./assets/js/classes/Streamer');
const DataStore                        = require('nedb');
const $                                = require("jquery");
const player                           = require('./player');
const settings                         = require('./settings');
const io                               = require('socket.io-client');

const encoder                          = new WebEmitter('./assets/js/workers/encoder.js');
const __unpacked                       = __dirname.replace('app.asar', 'app.asar.unpacked');

require('datejs');
let socket = null;
const YT = new Youtube('AIzaSyBYXfzGFQ6dyOgFztuv6-6n14Y0sc7oKJ4');
const context = new AudioContext();
const playlist = new DataStore({filename: path.join(__unpacked, 'db', 'playlist.json'), autoload: true});
const win = new WindowHandler(remote.getCurrentWindow());

let search_interval = null;
let lastVideoId = null;
setInterval(() => {
    const text = clipboard.readText();
    YT.match(text).then(id => {
        if (lastVideoId !== id) {
            lastVideoId = id;
            YT.get(lastVideoId).then(result => {
                $('.search_song').val(result[0].id).trigger('keyup');
            }).catch(error => {

            })
        }
    }).catch(error => {
    });
}, 3000);

playlist.find({}).sort({date: 1}).exec((error, documents) => {
    documents.forEach((value, index) => {
        player.enqueue(value);
    });
});


$('.progressbar').click(function (e) {
    const x = e.pageX - $(this).offset().left;
    const ratio = x / $(this).width();
    player.seek(player.duration * ratio);
});
$('.close').click(function () {
    win.close();
})
$('.playpause').click(function () {
    player.playPause();
})
$('.volume').on('change input', e => {
    player.gain(e.target.value / 100);
})
$('.close2').click(function () {
    $('.search_results').empty();
    $('.search_song').val('');
})
$('.prev').click(function () {
    player.previous();
})
$('.next').click(function () {
    player.next();
});
$('.playlist-button').click(function (e) {
    e.stopPropagation();
    $('.box_ss').toggleClass('active');
})
$('.speaker').click(function () {
    var active = $(this).hasClass('active');
    if (active) {
        player.speaker(false);
    } else {
        player.speaker(true);
    }
});
$('.stream').click(function () {
    const config = JSON.parse(fs.readFileSync(path.resolve(__dirname, '.', 'config', 'default.json')));
    if (config.token) {
        const active = $(this).hasClass('active');
        streamer.url('http://lounge.razorxan.it/stream/' + config.token);
        if (active) {
            streamer.stop();
            $(this).removeClass('active');
            socket.disconnect();
        } else {

            $(this).addClass('active');
            streamer.start();
            socket = io('ws://lounge.razorxan.it?token=' + config.token, {transports: ['websocket']});
            socket.on('error', function () {
                //console.log('error')
            })
            socket.on('connect', function () {
                //console.log('connected')
            });
            socket.on('disconnect', function () {
                //console.log('disconnected')
            });
            
        }
        return;
    }
    ipcRenderer.send('open_login');
});

$('.div_search').hide();
$('.search_song').keyup(function (e) {
    clearTimeout(search_interval);
    search_interval = setTimeout(() => {
        if ($.trim($(this).val()) === '') return;
        $('.search_song').attr('disabled', true);
        $('.lds-dual-ring').addClass('active');
        YT.search($(this).val(), 5).then(response => {
            $('.search_song').removeAttr('disabled').focus();
            $('.lds-dual-ring').removeClass('active');
            $('.div_search').show();
            $('.search_results').empty();
            response.forEach(function (item, index) {
                const h = $(`
                    <div class="box_single">
                        <div class="b_thumb">
                            <img src="assets/img/bk.jpg">
                        </div>
                        <div class="title"></div>
                    </div>
                `);
                h.data('id', item.id.videoId);
                h.find('img').attr('src', item.snippet.thumbnails.default.url);
                h.find('.title').text(item.snippet.title);
                h.click(function (e) {
                    e.stopPropagation();
                    download($(this).data('id'));
                    $('.box_ss').removeClass('active');
                    $('.search_results').empty();
                    $('.div_search').hide();
                    $('.search_song').val('');
                });
                $('.search_results').append(h);

                setTimeout(() => {
                    h.addClass('active');
                }, (index + 1) * 100);
            })
        }).catch(error => {
            $('.search_song').removeAttr('disabled').focus();
            $('.lds-dual-ring').removeClass('active');
        });
    }, 1000);
})
$('.always-on-top').click(function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        ipcRenderer.send('alwaysontop', false);
    } else {
        $(this).addClass('active');
        ipcRenderer.send('alwaysontop', true);
    }
});
$(document).click(function(e) {
    var container = $(".box_ss");
    var search = $('.search_song');
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.removeClass('active');
        if (!search.is(e.target) && search.has(e.target).length === 0) {
            $('.search_results').empty();
            $('.search_song').val('');
            $('.div_search').hide();
        }
    }
});


const notifications = new Worker('./assets/js/workers/notifications.js');
const downloadWorker = new WebEmitter('./assets/js/workers/download.js');
downloadWorker.on('loading', (id, url) => {
    ipcRenderer.send('notification', {
        title: 'Gathering Information',
        text: url
    });
}).on('downloadstart', (id, data) => {
    ipcRenderer.send('notification', {
        title: 'Download Started',
        text: data.meta.title,
        image: data.meta.thumbnail
    });
}).on('downloadprogress', (id, progress) => {
    $('.logs').text('Download at ' + progress + '%');
}).on('downloadend', (id, data) => {
    ipcRenderer.send('notification', {
        title: 'Download Complete',
        text: data.meta.title
    });
}).on('downloaderror', (id, message) => {
    ipcRenderer.send('notification', {
        title: 'An error has occurred',
        text: message
    });
}).on('conversionstart', (id, filename) => {
    $('.logs').fadeIn().text('Conversion started');
}).on('conversionprogress', (id, progress) => {
    if (progress > 100) progress = 100;
    $('.logs').text('Conversion at ' + progress + '%');
    setTimeout(function () {
        $('.logs').fadeOut();
    }, 1000);
}).on('conversionend', (id, filename, meta) => {
    player.enqueue({
        path: filename,
        meta
    });
    playlist.insert({
        path: filename,
        meta,
        date: Date.now()
    });
});

player.on('audiodata', event => {
    const buffer = event.inputBuffer;
    encoder.emit('mp3', {channels: [buffer.getChannelData(0), buffer.getChannelData(1)], sampleRate: buffer.sampleRate});
});

const streamer = new Streamer(encoder);

const download = val => {
    downloadWorker.emit('add_download', val);
};

require('./microphone')(player);