const $ = require("jquery");

module.exports = player => {
    let MICROPHONE = true;
    let MICROPHONE_MODE = 0; //PUSH TO TALK
    let MICROPHONE_PUSH = false;

    const MICROPHONE_MIN = 15;
    const MICROPHONE_MAX = 90;
    $(window).on('keydown', function (e) {
        if (e.which === 32) {
            MICROPHONE_PUSH = true;
        }
    }).on('keyup', function (e) {
        if (e.which === 32) {
            MICROPHONE_PUSH = false;
        }
    })
    navigator.webkitGetUserMedia({audio: true, video: false}, function (mic) {
        microphoneInput = player.context.createMediaStreamSource(mic);
        microphoneVolume = player.context.createGain();
        microphoneAnalyser = player.context.createAnalyser();
        microphoneAnalyser.fftSize = 1024;
        microphoneAnalyser.smoothingTimeConstant = 0.3;
        microphoneProcessor = player.context.createScriptProcessor(2048, 2, 2);
        microphoneProcessor.onaudioprocess = function (e) {
            var input = e.inputBuffer;
            var output = e.outputBuffer
            for (var channel = 0; channel < output.numberOfChannels; channel++) {
                var frequency =  new Uint8Array(microphoneAnalyser.frequencyBinCount);
                microphoneAnalyser.getByteFrequencyData(frequency);
                var average = getAverageVolume(frequency);
                var perc = (100 * average) / 80;
                $('#player-microphone-level').css({
                    width: perc + '%'
                });
                if ((perc > MICROPHONE_MIN && perc <= MICROPHONE_MAX && MICROPHONE_MODE === 1) || (MICROPHONE_MODE === 0 && MICROPHONE_PUSH)) {
                    $('#player-microphone-level').css({
                        backgroundColor: 'green'
                    });
                } else {
                    $('#player-microphone-level').css({
                        backgroundColor: 'red'
                    });
                }
                var inputData = input.getChannelData(channel);
                var outputData = output.getChannelData(channel);
                for (var sample = 0; sample < input.length; sample++) {
                    if ((perc > MICROPHONE_MIN && perc <= MICROPHONE_MAX && MICROPHONE_MODE === 1) || (MICROPHONE_MODE === 0 && MICROPHONE_PUSH)) {
                        outputData[sample] = inputData[sample];
                    } else {
                        outputData[sample] = 0;
                    }      
                }
            }
        };
        microphoneVolume.gain.value = 1; // AMPLIFY MICROPHONE VOLUME
        microphoneInput.connect(microphoneVolume);
        microphoneVolume.connect(microphoneAnalyser);
        microphoneVolume.connect(microphoneProcessor);
        microphoneProcessor.connect(player.processor);
    }, function (e) {
    });

    function getAverageVolume(array) {
        var values = 0;
        var average;
        var length = array.length;
        for (var i = 0; i < length; i++) {
            values += array[i];
        }
        average = values / length;
        return average;
    }

}
