const {app, Menu, Tray, BrowserWindow, ipcMain} = require('electron')
const path = require('path')
const url = require('url')
const fs = require('fs')

let mainWindow
let loginWindow
let tray = null
let Notify = null
const mode = (process.argv.indexOf('--dev') > -1) ? 'dev' : 'dist';
const windowOptions = {
	width: 270,
	height: 468,
	frame: false,
    transparent: true,
    maximizable: false,
	devTools: true,
	icon: path.join(__dirname, 'assets/icon/icon.png'),
	webPreferences: {
		nodeIntegrationInWorker: true
	}
}

if (mode === 'dev') {
	windowOptions.width = 800
	windowOptions.height = 600
	windowOptions.frame = true
} else {
	windowOptions.minWidth = 270
	windowOptions.minHeight = 468
	windowOptions.maxWidth = 270
	windowOptions.maxHeight = 468
	windowOptions.resizable = false
}
const shouldQuit = app.makeSingleInstance((commandLine, workingDirectory) => {
	if (mainWindow) {
		if (mainWindow.isMinimized()) mainWindow.restore()
		mainWindow.focus()
	}
})
if (shouldQuit) {
	app.quit()
	return
}
function createWindow () {
	Notify = require('electron-notify')
	Notify.setConfig({
		appIcon: path.join(__dirname, 'assets/icon/icon.png'),
		displayTime: 6000,
		defaultStyleClose: {
			position: 'absolute',
			top: 1,
			right: 3,
			fontSize: 11,
			color: '#CCC',
			cursor: 'pointer'
		  }
	})
	const contextMenu = Menu.buildFromTemplate(require(path.join(__dirname, '/assets/js/TrayMenu'))(app))
	tray = new Tray(path.join(__dirname, '/assets/icon/icon.png'))
	tray.setToolTip('ECM')
	tray.setContextMenu(contextMenu)
	tray.on('double-click', () => {
		mainWindow.show()
    })
    
	mainWindow = new BrowserWindow(windowOptions)
	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'index.html'),
		protocol: 'file:',
		slashes: true
	}))

	mainWindow.on('ready', () => {
		
	})

	
	if (mode === 'dev') {
		mainWindow.webContents.openDevTools()
	}
	
	mainWindow.on('closed', function () {
		app.quit()
		mainWindow = null
	})
	
}
ipcMain.on('alwaysontop', (event, flag) =>{
	mainWindow.setAlwaysOnTop(flag);
});

ipcMain.on('open_login', (event, arg) => {
	if (loginWindow) loginWindow.destroy();
	loginWindow = new BrowserWindow({
		parent: mainWindow,
		width: 270,
		height: 270,
		frame: false,
		transparent: true,
		devTools: true,
		icon: path.join(__dirname, 'assets/icon/icon.png'),
		webPreferences: {
			nodeIntegrationInWorker: true,
		}
	});
	loginWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'login.html'),
		protocol: 'file:',
		slashes: true
	}));
});
ipcMain.on('close_login', (event, arg) => {
	loginWindow.destroy();
	loginWindow = null;
});
ipcMain.on('notification', (event, arg) => {
	arg.image = arg.image || path.join(__dirname, 'assets/icon/icon.png')
	arg.sound = arg.sound || path.join(__dirname, 'assets/sound/notification.wav')
	Notify.notify(arg)
})

app.on('ready', createWindow);

app.on('window-all-closed', function () {
	if (process.platform !== 'darwin') {
		app.quit()
	}
})

app.on('activate', function () {
	if (mainWindow === null) {
		createWindow()
	}
})