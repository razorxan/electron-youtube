# electron-youtube

**Clone and run this repo for a quick test.**

Minimal Youtube music player based on electron. Atm only windows is supported.

## Quickstart

To clone and run this repository you'll need [Git](https://git-scm.com) [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. On windows [Microsoft Visual C++ 2010 Redistributable Package (x86)](https://www.microsoft.com/en-US/download/details.aspx?id=5555) is also required. From your command line:

You can use either yarn or npm

```bash
# Clone this repository
git clone https://gitlab.com/razorxan/electron-youtube
# Go into the repository
cd electron-quick-start
# Install dependencies
yarn
# Run the app
yarn start
```

## Commands

```bash
# Run the app
yarn start
# Run the app with devtools
yarn dev
# Build the app with asar archive and installer (uses electron-builder)
yarn dist
```