const ffbinaries = require('ffbinaries');
const path       = require('path');

const platform         = ffbinaries.detectPlatform();
const ffmpeg_filename  = ffbinaries.getBinaryFilename('ffmpeg', platform);
const ffprobe_filename = ffbinaries.getBinaryFilename('ffprobe', platform);
const ffplay_filename  = ffbinaries.getBinaryFilename('ffplay', platform);
const destination      = path.resolve(__dirname, '..', 'app', 'bin');

ffbinaries.downloadFiles(['ffmpeg', 'ffprobe', 'ffplay'], {destination}, (err, data) => {
    console.log('Downloaded Files: ', {
        'ffmpeg': path.resolve(destination, ffmpeg_filename),
        'ffprobe': path.resolve(destination, ffprobe_filename),
        'ffplay': path.resolve(destination, ffplay_filename)
    });
});