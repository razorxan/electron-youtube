const fs = require('fs');
const path = require('path');

fs.writeFileSync('./app/config/default.json', '{}');
fs.writeFileSync('./app/db/playlist.json', '');

fs.readdir('./app/download/', (err, files) => {
	if (err) throw err;
	for (const file of files) {
		if (path.extname(file) === '.mp3') {
			fs.unlinkSync(path.join('./app/download/', file));
		}
	}
});

fs.readdir('./app/download/tmp', (err, files) => {
	if (err) throw err;
	for (const file of files) {
		if (path.extname(file) === '.mp3') {
			fs.unlinkSync(path.join('./app/download/tmp', file));
		}
	}
});